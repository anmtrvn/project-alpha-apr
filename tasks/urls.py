from django.urls import path
from . import views


# URLConf
urlpatterns = [
    path("create/", views.createTask, name="create_task"),
    path("mine/", views.show_my_tasks, name="show_my_tasks"),
]
