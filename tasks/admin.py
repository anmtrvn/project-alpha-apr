from tasks.models import Task
from django.contrib import admin

# Register your models here.


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_project = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    )
